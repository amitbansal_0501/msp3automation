module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      files: ['Gruntfile.js', './**/*.js', '!./node_modules/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }

    },
    protractor: {
      options: {
        configFile: "node_modules/protractor/example/conf.js", // Default config file
        keepAlive: true, // If false, the grunt process stops when the test fails.
        noColor: false, // If true, protractor will not use colors in its output.
        args: {
          // Arguments passed to the command
        }
      },
      singlerun: { // Grunt requires at least one target to run so you can simply put 'all: {}' here too.
        options: {
          configFile: "config/conf.js", // Target-specific config file
          args: {} // Target-specific arguments
        }
      }
    },
    /*protractor: {
      options: {
        keepAlive: true,
        configFile: "config/conf.js"
        //"node_modules/protractor/example/conf.js"
          //configFile: "node_modules/protractor/example/conf.js"
      },
      singlerun: {
        options:
        {
          keepAlive: true,
          configFile: "config/conf.js"
        }
      },
      auto: {
        keepAlive: true,
        options: {
          args: {
           seleniumPort: 4444
          }
        }
      }
    },*/
    jsbeautifier: {
      beautify: {
        src: ['Gruntfile.js', './**/*.js', '!./node_modules/**/*.js'],
        options: {
          config: '.jsbeautifyrc'
        }
      },
      check: {
        src: ['Gruntfile.js', './**/*.js', '!./node_modules/**/*.js'],
        options: {
          mode: 'VERIFY_ONLY',
          config: '.jsbeautifyrc'
        }
      }
    }

  });

  // Load the plugins that will perform necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-jsbeautifier');
  grunt.loadNpmTasks('grunt-protractor-runner');

  /* Public grunt tasks - which you will call from command line */
  grunt.registerTask('default', ['jshint', 'jsbeautifier:beautify', 'protractor:singlerun']);
  grunt.registerTask('beautify', ['jsbeautifier:beautify']);
  grunt.registerTask('beautifycheck', ['jsbeautifier:check']);
  grunt.registerTask('test', ['protractor:singlerun']);
  grunt.registerTask('hint', ['jshint']);

};
