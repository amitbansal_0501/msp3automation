## How to set it up? ##

## Clone Repository ##
git clone https://amitbansal_0501@bitbucket.org/amitbansal_0501/msp3automation.git

## Install Node ##

Install Node (http://nodejs.org/)

## Install Dependendent Modules ##
npm install

## Install Grunt-cli ##
npm install -g grunt-cli --save-dev

## Install protractor globally and locally ##

npm install -g protractor


## Install grunt ##

npm install grunt --save-dev

## Install webdriver-manager locally for grunt to find it ##

node node_modules/protractor/bin/webdriver-manager update

## To run the test suite use ##

grunt default

