/**
 * Created by ABansal on 7/3/2015.
 */

var WaitHelper = function (webelement) {
  this.webElement = webelement;
};

WaitHelper.prototype.waitForElement = function () {
  browser.driver.wait(function () {
    return this.webElement.isElementPresent();
  }, 30000);
  console.log(this.webElement.isElementPresent()
    .toString());
};

module.exports = WaitHelper;
