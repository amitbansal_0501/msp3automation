/**
 * Created by ABansal on 7/2/2015.
 */
//'use strict';

var SelectWrapper = function (webelement) {
  this.webElement = webelement;
};
SelectWrapper.prototype.getOptions = function () {
  return this.webElement.findElement(by.tagName('option'));
};
SelectWrapper.prototype.getSelectedOptions = function () {
  return this.webElement.findElement(by.css('option[selected="selected"]'));
};
SelectWrapper.prototype.selectByValue = function (value) {
  return this.webElement.findElement(by.css('option[value="' + value + '"]'))
    .click();
};
SelectWrapper.prototype.selectByPartialText = function (text) {
  return this.webElement.findElement(by.cssContainingText('option', text))
    .click();
};
/*SelectWrapper.prototype.selectByText = function(text) {
    console.log("inside selectByText");
    return this.webElement.all(by.xpath('option[.="' + text + '"]')).click();
};*/

/*SelectWrapper.prototype.selectByText = function (text) {
    console.log(this.webElement.getId().toString());
    var str = '//select[@id=' + this.webElement.getId() +  ']/option[text()=' + text + ']';
    console.log(str);

    return this.webElement.findElement(by.xpath('//select[@id=' + this.webElement.getId() + ']/option[text()=' + text + ']')).click();
 /!* console.log("inside selectByText");
  console.log(this.webElement.isEnabled()
    .toString());
  //console.log(this.webElement.getLocation().toString());
  //return this.webElement.isDisplayed();
  //console.log(by.cssContainingText('option', text).toString());
  var obj = this.webElement.findElement(by.cssContainingText('option', text));
  console.log(obj.enabled()
    .toString());
  expect(obj.enabled())
    .toBeTruthy();
  return this.webElement.findElement(by.cssContainingText('option', text));*!/
};*/

module.exports = SelectWrapper;
