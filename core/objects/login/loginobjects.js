/**
 * Created by ABansal on 7/1/2015.
 */
var LoginObjects = function () {
  this.emailInput = browser.driver.findElement(by.id('DefaultContent_LoginControl_UserName'));
  this.passwordInput = browser.driver.findElement(by.id('DefaultContent_LoginControl_Password'));
  this.loginAction = browser.driver.findElement(by.id('DefaultContent_LoginControl_LoginButton_BImg'));
  this.loginBox = browser.driver.findElement(by.id('loginBox'));
};

module.exports = LoginObjects;
