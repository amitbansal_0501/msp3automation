/**
 * Created by ABansal on 7/2/2015.
 */
var LoginObjects2 = require('../../objects/login/loginobjects2.js');
var SelectWrapper = require('../../../helpers/selectwrapper.js');
var WaitHelper = require('../../../helpers/waithelpers.js');

var LoginPage2 = function (loginDataParams) {
  this.loginData = loginDataParams;
  this.loginObjects2 = new LoginObjects2();
};

LoginPage2.prototype.selectStudy = function () {
  var selectWrapper = new SelectWrapper(this.loginObjects2.studiesInput);
  selectWrapper.selectByValue(this.loginData.Study);
};

LoginPage2.prototype.selectMode = function () {
  var selectWrapper = new SelectWrapper(this.loginObjects2.modeInput);
  selectWrapper.selectByValue(this.loginData.Mode);
};

LoginPage2.prototype.submitAction = function () {
  this.loginObjects2.submitAction.click();
};

LoginPage2.prototype.isAt = function () {
  return this.loginObjects2.submitAction.isEnabled();
};

module.exports = LoginPage2;
