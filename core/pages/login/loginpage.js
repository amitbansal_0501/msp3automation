/**
 * Created by ABansal on 7/1/2015.
 */
var LoginObjects = require('../../objects/login/loginobjects.js');

var LoginPage = function (loginDataParams) {
  this.loginData = loginDataParams;
  this.loginObjects = new LoginObjects();
};

LoginPage.prototype.enterUserName = function () {
  this.loginObjects.emailInput.sendKeys(this.loginData.UserName);
};

LoginPage.prototype.enterPassword = function () {
  this.loginObjects.passwordInput.sendKeys(this.loginData.Password);
};

LoginPage.prototype.loginSubmit = function () {
  this.loginObjects.loginAction.click();
};

LoginPage.prototype.isAt = function () {
  return this.loginObjects.loginBox.isDisplayed();
};

LoginPage.prototype.isUserNameInputEnabled = function () {
  return this.loginObjects.emailInput.isEnabled();
};

LoginPage.prototype.isPasswordInputEnabled = function () {
  return this.loginObjects.passwordInput.isEnabled();
};

LoginPage.prototype.isSubmitActionEnabled = function () {
  return this.loginObjects.passwordInput.isEnabled();
};

module.exports = LoginPage;
