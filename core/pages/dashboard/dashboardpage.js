/**
 * Created by ABansal on 7/6/2015.
 */
var DashboardObjects = require('../../objects/dashboard/dashboardobjects.js');

var DashboardPage = function (loginDataParams) {
  this.loginData = loginDataParams;
  this.dashboardObjects = new DashboardObjects();
};

DashboardPage.prototype.isAt = function () {
  return this.dashboardObjects.portalNavigator.isDisplayed();
};

module.exports = DashboardPage;
