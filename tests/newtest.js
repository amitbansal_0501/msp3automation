var LoginPage = require('../core/pages/login/loginpage.js');
var LoginPage2 = require('../core/pages/login/loginpage2.js');
var DashboardPage = require('../core/pages/dashboard/dashboardpage.js');
var LoginData = require('../testdata/berlin-int/login/login.json');

describe('msp3 application login', function () {
  var loginPage = new LoginPage(LoginData);

  it('should login to MSP3', function () {
    browser.driver.get('https://expertportaldev.ert.com/msp3/int3/Login');
    expect(loginPage.isAt())
      .toBeTruthy();
  });

  it('should enter username', function () {
    expect(loginPage.isUserNameInputEnabled())
      .toBeTruthy();
    loginPage.enterUserName();
  });

  it('should enter password', function () {
    expect(loginPage.isPasswordInputEnabled())
      .toBeTruthy();
    loginPage.enterPassword();
  });
  var loginPage2 = new LoginPage2(LoginData);

  it('should click on submit button and study selection page should be loaded', function () {
    expect(loginPage.isSubmitActionEnabled())
      .toBeTruthy();
    loginPage.loginSubmit();
    expect(loginPage2.isAt());
  });

  it('should select study', function () {
    loginPage2.selectStudy();
    loginPage2.selectMode();
    loginPage2.submitAction();

    var dashboardPage = new DashboardPage(LoginData);
    expect(dashboardPage.isAt())
      .toBeTruthy();
  });

});
