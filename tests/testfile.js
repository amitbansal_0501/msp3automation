/**
 * Created by ABansal on 6/30/2015.
 */


var LoginPage = require('../core/pages/login/loginpage.js');
var LoginPage2 = require('../core/pages/login/loginpage2.js');
var LoginData = require('../testdata/berlin-int/login/login.json');
var DashboardPage = require('../core/pages/dashboard/dashboardpage.js');

describe('msp3 application login', function () {
  browser.driver.get('https://expertportaldev.ert.com/msp3/int3/Login');
  var loginPage = new LoginPage(LoginData);
  var loginPage2, dashboardPage;
  //console.log('test');

  it('should login to MSP3', function () {
    expect(loginPage.isAt())
      .toBeTruthy();
  });

  it('should enter username', function () {
    expect(loginPage.isUserNameInputEnabled())
      .toBeTruthy();

    loginPage.enterUserName();
  });

  it('should enter password', function () {
    expect(loginPage.isPasswordInputEnabled())
      .toBeTruthy();
    loginPage.enterPassword();
  });

  it('should click submit button', function () {
    expect(loginPage.isSubmitActionEnabled())
      .toBeTruthy();
    loginPage.loginSubmit();
  });
  it('should select study', function () {
    loginPage2 = new LoginPage2(LoginData);
    expect(loginPage2.isAt())
      .toBeTruthy();
    loginPage2.selectStudy();
  });

  it('should select mode', function () {
    //var loginPage2 = new LoginPage2(LoginData);
    loginPage2.selectMode();
  });

  it('should click on submit button', function () {
    loginPage2.submitAction();
    dashboardPage = new DashboardPage(LoginData);
    expect(dashboardPage.isAt())
      .toBeTruthy();
  });
});
