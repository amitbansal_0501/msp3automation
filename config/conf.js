/**
 * Created by ABansal on 6/30/2015.
 */
//var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
//var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

exports.config = {
  seleniumAddress: '',

  capabilities: {
    "browserName": 'chrome'
  },

  framework: 'jasmine2',

  onPrepare: function () {
    browser.manage()
      .timeouts()
      .pageLoadTimeout(40000);

    browser.manage()
      .timeouts()
      .implicitlyWait(5000);
    browser.ignoreSynchronization = true;

    /*jasmine.getEnv()
      .addReporter(new HtmlScreenshotReporter({
        dest: 'reports/screenshots',
        filename: 'my-report.html'
          /!* pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {

              var monthMap = {
              "1": "Jan",
              "2": "Feb",
              "3": "Mar",
              "4": "Apr",
              "5": "May",
              "6": "Jun",
              "7": "Jul",
              "8": "Aug",
              "9": "Sep",
              "10": "Oct",
              "11": "Nov",
              "12": "Dec"
              };

            var currentDate = new Date(),
               currentHoursIn24Hour = currentDate.getHours(),
               currentTimeInHours = currentHoursIn24Hour > 12 ? currentHoursIn24Hour - 12 : currentHoursIn24Hour,
               totalDateString = currentDate.getDate() + '-' + monthMap[currentDate.getMonth() + 1] + '-' + (currentDate.getYear() + 1900) +
               '-' + currentTimeInHours + 'h-' + currentDate.getMinutes() + 'm';

             return path.join(totalDateString, capabilities.caps_.browserName, descriptions.join('-'));
           }*!/
      }));*/

    /*jasmine.getEnv()
      .addReporter(new HtmlReporter({
        baseDirectory: 'tmp/screenshots',
        /!*pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {
          // Return '<browser>/<specname>' as path for screenshots:
          // Example: 'firefox/list-should work'.
          return path.join(capabilities.caps_.browser, descriptions.join('-'));
        },*!/
        docName: 'testreport.html',
        docTitle: 'MSP3 Test Results',
        takeScreenShotsOnlyForFailedSpecs: false,
        takeScreenShotsForSkippedSpecs: true,
        preserveDirectory: true

        //,cssOverrideFile: 'css/style.css'
      }));*/
    /*var mkdirp = require('mkdirp');
    var jasmineReporters = require('jasmine-reporters');

    var folderName = (new Date())
      .toString()
      .replace(/:/g, ' ')
      .split(' ')
      .splice(1, 6)
      .join(' ');

    var newFolder = "./reports/" + folderName;

    mkdirp(newFolder, function (err) {
      if (err) {
        console.error(err);
      } else {
        jasmine.getEnv()
          .addReporter(new jasmineReporters.JUnitXmlReporter({
            savePath: newFolder,
            consolidateAll: true,
            filePrefix: 'outputresults'
          }));
      }
    });*/

    browser.driver.manage()
      .window()
      .maximize();
  },

  // ----- Options to be passed to minijasminenode -----
  jasmineNodeOpts: {
    // onComplete will be called just before the driver quits.
    onComplete: null,
    // If true, display spec names.
    isVerbose: false,
    // If true, print colors to the terminal.
    showColors: true,
    // If true, include stack traces in failures.
    includeStackTrace: true
      // Default time to wait in ms before a test fails.
      //defaultTimeoutInterval: 10000
  },

  suites: {
    iq: ['../tests/newtest.js']
  },

  specs: ['../tests/testfile.js']
};


/* multiCapabilities: [
 {browserName: 'firefox'},
 {browserName: 'chrome'}
 ],*/
